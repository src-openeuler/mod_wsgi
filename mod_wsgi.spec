%{!?_httpd_apxs: %{expand: %%global _httpd_apxs %%{_sbindir}/apxs}}
%{!?_httpd_mmn: %{expand: %%global _httpd_mmn %%(cat %{_includedir}/httpd/.mmn 2>/dev/null || echo 0-0)}}
%{!?_httpd_confdir:    %{expand: %%global _httpd_confdir    %%{_sysconfdir}/httpd/conf.d}}
%{!?_httpd_modconfdir: %{expand: %%global _httpd_modconfdir %%{_sysconfdir}/httpd/conf.d}}
%{!?_httpd_moddir:    %{expand: %%global _httpd_moddir    %%{_libdir}/httpd/modules}}
%global sphinxbin %{_bindir}/sphinx-build-3
Name:                mod_wsgi
Version:             5.0.0
Release:             1
Summary:             A WSGI interface for Python web applications in Apache
License:             Apache-2.0
URL:                 https://modwsgi.readthedocs.io/
Source0:             https://github.com/GrahamDumpleton/mod_wsgi/archive/refs/tags/%{version}.tar.gz
Source1:             wsgi-python3.conf
Patch1:              mod_wsgi-4.5.20-exports.patch
BuildRequires:       httpd-devel gcc perl chrpath
%{?filter_provides_in: %filter_provides_in %{_httpd_moddir}/.*\.so$}
%{?filter_setup}
%description
The mod_wsgi adapter is an Apache module that provides a WSGI compliant
interface for hosting Python based web applications within Apache. The
adapter is written completely in C code against the Apache C runtime and
for hosting WSGI applications within Apache has a lower overhead than using
existing WSGI adapters for mod_python or CGI.

%package -n python3-%{name}
Summary:             %summary
Requires:            httpd-mmn = %{_httpd_mmn}
BuildRequires:       python3-devel, python3-sphinx
Provides:            mod_wsgi = %{version}-%{release}
Provides:            mod_wsgi%{?_isa} = %{version}-%{release}
Obsoletes:           mod_wsgi < %{version}-%{release}
%description -n python3-%{name}
The mod_wsgi adapter is an Apache module that provides a WSGI compliant
interface for hosting Python based web applications within Apache. The
adapter is written completely in C code against the Apache C runtime and
for hosting WSGI applications within Apache has a lower overhead than using
existing WSGI adapters for mod_python or CGI.


%prep
%autosetup n %{name}-%{version} -p1

%build
make -C docs html SPHINXBUILD=%{sphinxbin}
export LDFLAGS="$RPM_LD_FLAGS -L%{_libdir}"
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
mkdir py3build/
cp -R * py3build/ || :
pushd py3build
%configure --enable-shared --with-apxs=%{_httpd_apxs} --with-python=python3
make %{?_smp_mflags}
%py3_build
popd

%install
pushd py3build
make install DESTDIR=$RPM_BUILD_ROOT LIBEXECDIR=%{_httpd_moddir}
mv  $RPM_BUILD_ROOT%{_httpd_moddir}/mod_wsgi{,_python3}.so
install -d -m 755 $RPM_BUILD_ROOT%{_httpd_modconfdir}
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_httpd_modconfdir}/10-wsgi-python3.conf

%py3_install
mv $RPM_BUILD_ROOT%{_bindir}/mod_wsgi-express{,-3}
popd

chrpath -d %{buildroot}%{python3_sitearch}/mod_wsgi/server/mod_wsgi-py3*.cpython-3*-%{_arch}-linux-gnu.so

%files -n python3-%{name}
%license LICENSE
%doc CREDITS.rst README.rst
%config(noreplace) %{_httpd_modconfdir}/*wsgi-python3.conf
%{_httpd_moddir}/mod_wsgi_python3.so
%{python3_sitearch}/mod_wsgi-*.egg-info
%{python3_sitearch}/mod_wsgi
%{_bindir}/mod_wsgi-express-3

%changelog
* Mon Feb 26 2024 liyanan <liyanan61@h-partners.com> - 5.0.0-1
- Update to version 5.0.0

* Sun Jul 23 2023 yaoxin <yao_xin001@hoperun.com> - 4.9.4-2
- Fix compilation failure  caused by python update to 3.11.4

* Thu May 11 2023 Ge Wang <wang__ge@126.com> - 4.9.4-1
- Update to version 4.9.4

* Fri Mar 3 2023 liyanan  <liyanan32@h-partners.com> - 4.9.1-3
- Remove rpath

* Mon Aug 08 2022 zhuhai95 <zhuhai@ncti-gba.cn> - 4.9.1-2
- Fix CVE-2022-2255

* Tue May 17 2022 yangping <yangping69@h-partners> - 4.9.1-1
- Update to 4.9.1

* Sat Feb 27 2021 zhaorenhai <zhaorenhai@hotmail.com> - 4.6.4-2
- Add configure file

* Thu Nov 19 2020 huanghaitao <huanghaitao8@huawei.com> - 4.6.4-1
- package init
